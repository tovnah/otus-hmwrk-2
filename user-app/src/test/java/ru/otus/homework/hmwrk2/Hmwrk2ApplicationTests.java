package ru.otus.homework.hmwrk2;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@Disabled
@SpringBootTest
class Hmwrk2ApplicationTests {

    @Test
    void contextLoads() {
    }

}
