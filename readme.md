Домашнее задание `Основы работы с Kubernetes (часть 3)`
- выполнить в корневой директории ```helm install otus-hmwrk-2 ./otus-hmwrk-2-chart```
- выполнить проверку CRUD операций ```newman run postman/otus_homework2.postman_collection.json```
